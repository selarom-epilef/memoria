 # State of the art
* Effort estimation methods
  * Expert methods
  * Parametric methods
* Feature engineering
* Regression models
* Probability distribution generation methods (aka sampling and others)

# Proposed solution
* Define tags and their respective hyphothesis / argument for trial

# Results
* Tagging
* Build feature engineering code
* Get results from feature engineering
* Build regression models testing code
* Run regression model selection experiments
* Build sampling / probability distribution generation experiments
* Choose better methods and most representative

# Conclusions
