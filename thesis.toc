\babel@toc {spanish}{}
\babel@toc {spanish}{}
\contentsline {chapter}{Agradecimientos}{\es@scroman {iii}}{chapter*.1}
\contentsline {chapter}{Resumen}{\es@scroman {iv}}{chapter*.2}
\contentsline {chapter}{Abstract}{\es@scroman {v}}{chapter*.3}
\contentsline {chapter}{\IeC {\'I}ndice de Contenidos}{\es@scroman {vi}}{chapter*.4}
\contentsline {chapter}{Glosario}{\es@scroman {x}}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Objetivo general}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Objetivos espec\IeC {\'\i }ficos}{2}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Metodolog\IeC {\'\i }a}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Estructura de la memoria}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}Problema y Contexto}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Marco Te\IeC {\'o}rico}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Metodolog\IeC {\'\i }as contempor\IeC {\'a}neas de desarrollo}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Modelo cl\IeC {\'a}sico}{10}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Modelo \IeC {\'a}gil}{12}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}M\IeC {\'a}quinas de aprendizaje }{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Entrenamiento}{15}{subsection.3.2.1}
\contentsline {subsubsection}{\textit {Cross-Validation} }{15}{section*.8}
\contentsline {subsection}{\numberline {3.2.2}\textit {Overfitting} y \textit {underfitting}}{16}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Sesgo versus varianza}{16}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Maldici\IeC {\'o}n de la dimensionalidad}{17}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Regularizaci\IeC {\'o}n }{18}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Ingenier\IeC {\'\i }a de atributos}{19}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Transformaci\IeC {\'o}n de atributos}{20}{subsection.3.3.1}
\contentsline {subsubsection}{Datos num\IeC {\'e}ricos}{20}{section*.11}
\contentsline {subsubsection}{Datos categ\IeC {\'o}ricos}{21}{section*.12}
\contentsline {subsubsection}{Normalizaci\IeC {\'o}n de atributos}{22}{section*.13}
\contentsline {subsection}{\numberline {3.3.2}Selecci\IeC {\'o}n de atributos}{23}{subsection.3.3.2}
\contentsline {subsubsection}{M\IeC {\'e}todos envolventes}{24}{section*.15}
\contentsline {subsubsection}{M\IeC {\'e}todos de filtrado}{26}{section*.17}
\contentsline {subsubsection}{M\IeC {\'e}todos incrustados}{26}{section*.18}
\contentsline {subsection}{\numberline {3.3.3}\textit {Principal Component Analysis} (PCA)}{27}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Modelos lineales de regresi\IeC {\'o}n}{28}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}M\IeC {\'\i }nimos cuadrados ordinarios}{29}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Lasso}{30}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Modelos no lineales de regresi\IeC {\'o}n}{30}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}M\IeC {\'a}quinas de soporte}{30}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}\IeC {\'A}rboles de regresi\IeC {\'o}n }{35}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Optimizaci\IeC {\'o}n de modelos}{38}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Tuneo de hiperpar\IeC {\'a}metros}{39}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}M\IeC {\'e}todos de ensamblaje}{40}{subsection.3.6.2}
\contentsline {subsubsection}{\textit {Bagging}}{41}{section*.27}
\contentsline {subsubsection}{\textit {Random forest}}{42}{section*.29}
\contentsline {subsubsection}{\textit {AdaBoost}}{43}{section*.31}
\contentsline {subsubsection}{\textit {Gradient Boosting}}{44}{section*.33}
\contentsline {section}{\numberline {3.7}Estimaci\IeC {\'o}n de distribuci\IeC {\'o}n por m\IeC {\'e}todo de kernel }{45}{section.3.7}
\contentsline {chapter}{\numberline {4}Estado del Arte}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}M\IeC {\'e}todos de estimaci\IeC {\'o}n de esfuerzo}{47}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Estimaci\IeC {\'o}n por evaluaci\IeC {\'o}n experta}{48}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Estimaci\IeC {\'o}n basada en analog\IeC {\'\i }as}{51}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Modelos param\IeC {\'e}tricos}{51}{subsection.4.1.3}
\contentsline {subsubsection}{COCOMO}{52}{section*.36}
\contentsline {subsubsection}{SLIM}{56}{section*.39}
\contentsline {subsubsection}{SEER-SEM}{57}{section*.41}
\contentsline {subsection}{\numberline {4.1.4}Aprendizaje autom\IeC {\'a}tico}{58}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Caracter\IeC {\'\i }sticas utilizadas para la estimaci\IeC {\'o}n}{61}{section.4.2}
\contentsline {section}{\numberline {4.3}M\IeC {\'e}tricas de rendimiento}{63}{section.4.3}
\contentsline {chapter}{\numberline {5}Soluci\IeC {\'o}n Propuesta}{65}{chapter.5}
\contentsline {section}{\numberline {5.1}Etapa de pre-procesamiento}{67}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}An\IeC {\'a}lisis de atributos }{68}{subsection.5.1.1}
\contentsline {subsubsection}{Atributos en bruto}{68}{section*.45}
\contentsline {subsubsection}{Atributos propuestos}{72}{section*.48}
\contentsline {subsection}{\numberline {5.1.2}Agregaci\IeC {\'o}n de atributos}{75}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Definici\IeC {\'o}n de conjuntos }{79}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Normalizaci\IeC {\'o}n y filtrado}{80}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Etapa de selecci\IeC {\'o}n}{81}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Selecci\IeC {\'o}n de hiperpar\IeC {\'a}metros}{81}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Selecci\IeC {\'o}n de modelos}{82}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Modelos propuestos}{83}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Etapa de pron\IeC {\'o}stico}{86}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Distribuci\IeC {\'o}n del valor esperado del estimador}{86}{subsection.5.3.1}
\contentsline {chapter}{\numberline {6}Validaci\IeC {\'o}n y Resultados}{87}{chapter.6}
\contentsline {section}{\numberline {6.1}Importancia de los atributos}{88}{section.6.1}
\contentsline {section}{\numberline {6.2}Rendimiento de la soluci\IeC {\'o}n}{93}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Comportamiento de selecci\IeC {\'o}n de modelos}{94}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Rendimiento del framework seg\IeC {\'u}n atributos utilizados}{95}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Relaci\IeC {\'o}n entre error y cantidad de datos}{99}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Distribuciones obtenidas}{101}{subsection.6.2.4}
\contentsline {chapter}{Conclusiones}{103}{chapter*.71}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{106}{chapter*.72}
\contentsline {chapter}{Anexos}{111}{chapter*.73}
\contentsline {section}{\numberline {A}Proceso de reporte de esfuerzo actual en la empresa Nursoft }{111}{section.6.1}
\contentsline {section}{\numberline {B}Caracter\IeC {\'\i }sticas generales de los atributos propuestos luego del proceso de agregaci\IeC {\'o}n }{112}{section.6.2}
\contentsline {section}{\numberline {C}Comportamiento del tama\IeC {\~n}o del conjunto de entrenamiento a trav\IeC {\'e}s del tiempo }{114}{section.6.3}
