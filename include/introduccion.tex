\chapter{Introducción}
El éxito en los proyectos de desarrollo de software depende del equilibrio de 3 variables fundamentales: el costo, el alcance y su duración. Esta última debe ser definida al inicio de los proyectos independientemente de la metodología de desarrollo que se esté utilizando, y sin embargo es la que presenta la mayor tasa de problemas en la industria. Alrededor del  60\% de los proyectos no logra finalizar dentro de los plazos que inicialmente fueron propuestos \cite{chaosreport}, lo cual se explica por el gran nivel de incertidumbre que se tiene al inicio de ellos en comparación con estimaciones realizadas más tarde en el desarrollo \cite{boehm_1981}.

La estimación del tiempo de un proyecto se traduce en estimar el \textbf{esfuerzo} total requerido para realizarlo, que se define como la cantidad de horas humanas que se deben invertir para desarrollar todas las funcionalidades que lo componen.  El proceso de pronóstico del esfuerzo ha sido exhaustivamente estudiado desde varios enfoques, con métodos y metodologías asociadas a juicios expertos, modelos paramétricos y aprendizaje automático  \cite{POSPIESZNY2018184}, teniendo la mayoría de estas investigaciones problemas debido a la antigüedad de los datos que se analizan, a la baja presencia de ellas en la industria, y por último y más importante, a su reducida aplicabilidad a desarrollos que utilizan metodologías ágiles, las cuales poco a poco se han convertido en un estándar común en la industria. Lo anterior se debe principalmente debido a que se utilizan datos de los proyectos que no están disponibles al inicio en metodologías ágiles, en donde la planificación tiende a ser cortoplacista y no suele haber un rumbo claro sobre la totalidad de funcionalidades que se implementarán. Surge entonces la necesidad de explorar nuevas formas compatibles con el agilismo para predecir el esfuerzo. Es también relevante considerar que lo único certero respecto a la estimación de esfuerzo en proyectos de software, es que en definitiva ésta jamás será certera \cite{Trendowicz2014}. Por lo tanto, es de esperar que todas las estimaciones de esfuerzo tengan un grado de incertidumbre en sus pronósticos. Luego, es necesario acompañar el desarrollo de un método predictivo junto a la definición de una estrategia para transparentar esta incertidumbre, sobre todo considerando que es importante que los clientes dueños de los proyectos se puedan alinear con la idea de procesos inciertos \cite{bakalova2011comparative}.

% Dichas  funcionalidades se documentan en la actualidad como historias de usuario \textit{(user stories)}, que corresponden a breves descripciones en lenguaje natural de lo que se necesita realizar.

En este trabajo se utilizó un dataset actualizado de proyectos de software sobre el cual se propuso un método de aprendizaje automático para realizar pronósticos del esfuerzo requerido para completarlos, basado en las historias de usuario de cada uno. Estas últimas corresponden a la forma de documentar funcionalidades que se sugiere en las metodologías ágiles modernas. El dataset tiene en total $12$ proyectos de la empresa de desarrollo de software \textit{Nursoft} y $326$ historias de usuario con atributos en bruto presentes en la base de datos donde residen, a los cuales se referenciará de ahora en adelante como \textbf{atributos base}. Sobre estos se propuso además la incorporación de nueva información que mejore la precisión del pronóstico completo.


\section{Objetivos}
\subsection{Objetivo general}
\begin{itemize}
\item Validar un método de aprendizaje automático para pronosticar al inicio de proyectos de desarrollo de software el esfuerzo requerido para finalizarlos, a partir de atributos relevantes propuestos de las historias de usuario que los componen.
\end{itemize}
\subsection{Objetivos específicos}
\begin{itemize}
    \item Construir una herramienta que permita a Nursoft agilizar futuras investigaciones sobre estimación de esfuerzo usando aprendizaje automático.

    \item Explorar y seleccionar distintos modelos de aprendizaje para realizar el pronóstico.

    \item Evaluar el impacto de la ingeniería de atributos en el rendimiento de la solución propuesta.
    
    \item Definir una estrategia para estimar la incertidumbre del pronóstico.

    \item Mejorar el rendimiento de la metodología actual basada en Planning Poker utilizada en Nursoft.
    
    \item Comparar el rendimiento obtenido con investigaciones similares del estado del arte.
    \end{itemize}

\section{Metodología}

La metodología utilizada en el presente trabajo se basa en CRISP-DM, propuesta por Wirth et al. \cite{wirth2000crisp}, que define una serie de pasos que permiten construir y evaluar modelos sobre conjuntos de datos que se desean explorar. Se definen en ésta las etapas de entendimiento del negocio, entendimiento de los datos, preparación de los datos, modelamiento de los datos, evaluación del modelo y la puesta en producción de los modelos.

En primer lugar, la etapa de entendimiento del negocio se realiza explorando el estado del arte de estimación del esfuerzo en proyectos de software y realizando un análisis del proceso actual que realiza la empresa de la cual provienen los datos.

Luego, y para poder entender la estructura de los historias de usuario que se utilizarán para realizar predicciones, se realiza un análisis de los atributos de las historias de usuario disponibles en los proyectos, enfocándose en la utilidad que tienen estos en los pronósticos. Se proponen además un conjunto nuevo de atributos relevantes basados en las características más importantes que afectan el esfuerzo requerido para realizar funcionalidades.

A continuación, se agregan los atributos propuestos previamente a las historias de usuario y se transforman estas a formatos que se pueden utilizar en técnicas de aprendizaje automático.

Subsiguientemente, se exploran, evalúan y seleccionan distintos modelos de aprendizaje automático para construir el \textit{framework} de pronósticos de esfuerzo, haciendo énfasis en modelos que realicen selección de atributos de manera inherente para poder visualizar y analizar la importancia de cada uno de los atributos utilizados para el pronóstico de un proyecto.

Posteriormente, se exploran y refinan métodos para poder obtener distribuciones de probabilidad para visualizar la incertidumbre del pronosticador seleccionado durante el pronóstico de un proyecto. 

Finalmente, se analizan los resultados finales de los pronósticos obtenidos realizando simulaciones con los proyectos terminados en la empresa \textit{Nursoft} y se realiza una comparación con el estado del arte y el proceso actual de la empresa, utilizando métricas estándar en el rubro.

\section{Estructura de la memoria}
En el capítulo 2 se define con más detalle la problemática a tratar y el contexto en el cual se estará validando el método propuesto, explicando la procedencia de los datos que se utilizarán para realizar el pronóstico del esfuerzo y los procesos internos actuales que se usan para pronosticar.

En el capítulo 3 se resume la literatura referente a metodologías ágiles y máquinas de aprendizaje, explicando cómo se pre-procesan los datos, cómo se describen los modelos probados para la predicción y cómo son estos seleccionados. Además, se explican los métodos utilizados para visualizar la incertidumbre de los pronósticos.

En el capítulo 4 se hace una revisión exhaustiva del estado del arte de la problemática de predecir el esfuerzo de proyectos de software, haciendo énfasis en los distintos métodos para realizar pronósticos, en los atributos y características interesantes que utilizan los estudios para sus pronósticos y en las métricas estándares definidas en el rubro para realizar comparaciones correctas.

En el capítulo 5 se presentan los resultados obtenidos al realizar pronósticos con el \textit{framework} y se visualiza el rendimiento obtenido.

Finalmente, en las conclusiones se analizan los resultados obtenidos, realizando una comparación con el estado del arte y el proceso actual utilizado en la empresa de donde provienen los datos.
