\chapter{Validación y Resultados}
Para validar el \textit{framework} definido en el capítulo anterior, se realizará el pronóstico de todos los proyectos existentes en la empresa de desarrollo \textit{Nursoft} un total de $20$ veces, a excepción de $2$ proyectos en particular que se dejarán como base para poder realizar los pronósticos iniciales. Dichos proyectos corresponden a los primeros en un marco ordenado temporalmente y a los cuales es necesario aislar para que el primer proyecto que se quiere pronosticar pueda tener un conjunto de entrenamiento $T_{train}$. En cada iteración, se realizarán 3 pronósticos distintos para cada proyecto, en donde en uno se considerarán solamente los atributos propuestos, en otro solamente los atributos en bruto de las historias de usuario y en el último la combinación de todo.

Una vez obtenidos los resultados de cada iteración sobre los $10$ proyectos restantes, luego de no considerar los proyectos base previamente definidos, se realizará un análisis respecto a la importancia que tiene cada uno de los atributos de las historias de usuario en el pronóstico del esfuerzo de los proyectos, se contrastarán los resultados obtenidos al pronosticar con y sin los atributos propuestos, y por último, se utilizarán las métricas de $PRED(25)$ y $MMRE$ para comparar el rendimiento del \textit{framework} con el estado del arte actual y con el proceso de pronóstico basado completamente en \textit{Planning Poker} que se utiliza actualmente en la empresa.



\section{Importancia de los atributos}
Los atributos propuestos obtenidos luego del proceso de agregación son en total $159$, de los cuales $2\%$ son booleanos, $3\%$ numéricos y el restante $95\%$ son atributos categóricos transformados a través de un proceso \textit{One-hot}. Sus características se encuentran descritos en el anexo \ref{data_characteristics}.

Para poder obtener la importancia de los atributos durante el pronóstico, se utilizó el promedio de pesos asignados a cada uno de ellos en todos los árboles utilizados por los métodos de ensamblado seleccionado en cada proyecto. La importancia de aquellos atributos con un valor significativo de importancia pueden ser visualizados en el siguiente mapa de calor:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res4_feature_importance.png}
        \caption{Importancia de atributos por proyecto}
        \label{res4}
\end{figure}

Esta información representa la importancia promedio de los atributos en los árboles del ensamblado en cada una de las $20$ iteraciones. Es evidente notar que el atributo que presenta el mayor grado de importancia en las decisiones de cada árbol del ensamblado es el de \textit{total\_points} en todos los proyectos. Este atributo corresponde al puntaje asignado por el comité evaluador durante el proceso de \textit{Planning Poker}.

La importancia general de los atributos promedio de todos los proyectos se puede visualizar en la siguiente figura:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res7_tag_general_importance.png}
        \caption{Importancia general de los atributos}
        \label{res6}
\end{figure}

En donde se hace aún más evidente que es \textit{total\_points} el atributo más importante para realizar el pronóstico y que en contraste con el resto de los atributos tiene un valor de importancia porcentual al menos 2 órdenes de magnitud mayor.

Para poder visualizar el resto de los atributos de mejor manera, se elimina \textit{total\_points} del mapa de calor pro proyecto, obteniéndose lo siguiente:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res5_tag_importance.png}
        \caption{Importancia de atributos por proyecto, sin considerar el puntaje asignado por el comité}
        \label{res5}
\end{figure}

En esta visualización es posible detectar la importancia de los atributos propuestos en las predicciones, en donde se tiene que no existe un atributo que sea muy importante en todos los proyectos de desarrollo. A nivel general, se tiene lo siguiente:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res6_feature_general_important.png}
        \caption{Importancia general de los atributos, sin considerar el puntaje asignado por el comité}
        \label{res7}
\end{figure}

Se puede notar que en promedio el atributo de complejidad es el que tiene mayor importancia con un valor cercano a $4\%$, lo cual sigue siendo marginal en comparación a la importancia de \textit{total\_points} que tiene un valor aproximado de $66\%$ de importancia.

Si se consideran pronósticos realizados utilizando solo los atributos propuestos, se obtiene lo siguiente:


\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res_tags_only_heatmap.png}
        \caption{Importancia de atributos al pronosticar solo con los atributos propuestos, desagregado por proyecto}
        \label{res_tags_only_heatmap}
\end{figure}

Se observa nuevamente que el atributo predominante para realizar las estimaciones corresponde a la complejidad. Dicho atributo es el que mejor representa de manera proporcional cuán grande será una historia de usuario o no con una baja precisión, debido a que corresponde a un número incremental pero subjetivo. En promedio, la importancia se puede ver reflejada en el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res_tags_only_bar.png}
        \caption{Importancia promedio de atributos al pronosticar solo con los atributos propuestos}
        \label{res_tags_only_bar}
\end{figure}

En donde se puede observar que el atributo relevante que le sigue a la complejidad, es la demanda de Adaptabilidad en los proyectos. Cuando se tiene adaptabilidad como requerimiento no funcional, es necesario generalizar aún más las soluciones de software que se generan y dedicar tiempo a decisiones de bajo nivel que permitan responder al cambio fácilmente. Lo anterior, hace que las historias de usuario tiendan a demorar un poco más que los demás.

\section{Rendimiento de la solución}
A continuación se presentan los resultados obtenidos en torno al comportamiento general del \textit{framework}, su rendimiento en comparación con el método actual que se utiliza para pronosticar el esfuerzo de proyectos en la empresa y su rendimiento general en contraste con resultados contemporáneos definidos en el estado del arte.

\subsection{Comportamiento de selección de modelos}
Los modelos candidatos propuestos en la sección anterior tienen una frecuencia de selección reflejado en el siguiente mapa de calor:

\begin{figure}[H]
    \centering
        \includegraphics[width=.8\linewidth]{images/res2_model_usage.png}
        \caption{Frecuencia de selección de los modelos pro proyecto}
        \label{res2}
\end{figure}

Se puede observar que la selección de modelos es relativamente consistente en la mayoría de los proyectos, a excepción del proyecto \textit{mediateur} en donde se selecciona de forma casi aleatoria entre \textit{Random Forest} y \textit{Extreme Random Forest}. 

En promedio, los modelos seleccionados corresponden a los siguientes:

\begin{figure}[H]
    \centering
        \includegraphics[width=.8\linewidth]{images/res3_general_model_usage.png}
        \caption{Frecuencia de selección general de los modelos}
        \label{res3}
\end{figure}

Se puede notar que el modelo de ensamblado seleccionado de mayor frecuencia es \textit{Random Forest}, siguiéndole \textit{AdaBoost}.

\subsection{Rendimiento del framework según atributos utilizados}
Con ánimo de ver el efecto de los atributos propuestos en el rendimiento general del \textit{framework}, se tiene el siguiente diagrama comparativo del error relativo ($MRE$):

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res_compare_all3.png}
        \caption{Comparación de rendimiento entre conjunto de atributos utilizados para el pronóstico. A la izquierda está el rendimiento al utilizar solo los atributos base, al centro solo los atributos propuestos y a la derecha la combinación. El eje $Y$ corresponde al $MRE$ promedio entre todas las iteraciones de los proyectos pronosticados, por tanto, mientras más alto es peor. La línea roja que se puede observar es el limite de $MRE = 25\%$ que es utilizado por la métrica de $PRED(25)$}
        \label{res_compare_all3}
\end{figure}

Se puede observar en la figura \ref{res_compare_all3} que en términos de la métrica de $PRED(25)$, el rendimiento de los pronósticos es mejor cuando no se incorporan los atributos propuestos. Esto se puede visualizar aún más considerando el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res9_pred25.png}
        \caption{Comparación de PRED(25). En rojo se encuentra el valor obtenido con atributos base, en azul con los atributos propuestos, en morado con la combinación y en gris utilizando la metodología actual de la empresa (\textit{Planning poker})}
        \label{res9}
\end{figure}

Se puede observar en la figura \ref{res9} que el rendimiento del framework utilizando solo los atributos propuestos tiene un rendimiento inferior al \textit{Planning poker} y un aumento de este al incorporar los atributos base, siendo el rendimiento aún mejor cuando no se utilizan los atributos propuestos. Es importante notar que el cálculo del PRED(25) en un dataset de proyectos acotado es muy punitivo, haciendo que se perciba una gran disminución de rendimiento cuando un proyecto en particular no es pronosticado correctamente.

El error relativo promedio de los pronósticos se encuentra resumido en el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res10_mmre.png}
        \caption{Comparación de MMRE. En rojo se encuentra el valor obtenido con atributos base, en azul con los atributos propuestos, en morado con la combinación y en gris utilizando la metodología actual de la empresa (\textit{Planning poker})}
        \label{res10}
\end{figure}

Es muy relevante hacer énfasis en que el error relativo promedio es muy similar entre la utilización de los atributos base y la combinación con los propuestos. Esto da nociones de que la incorporación de los atributos propuestos añade inestabilidad al framework. Para explorar esto, se tiene lo siguiente:

\begin{table}[H]
    \centering
    \begin{tabular}{|p{.3\linewidth}|l|l|}
    
    \hline
     & Base & Combinación \\ \hline
    Varianza de MMRE general & 0.1095     & 0.1629      \\ \hline
    Promedio de la varianza de MRE en cada iteración & 0.1199 & 0.1778 \\ \hline
    \end{tabular}
\end{table}

Estos resultados indican que la incorporación de los atributos propuestos aumentan la varianza del $MRE$ de los pronósticos, lo que confirma la inestabilidad adicional.

\subsection{Relación entre error y cantidad de datos}

Uno de los desafíos más importantes a la hora de realizar los pronósticos es la poca cantidad de datos que se tiene en el momento. Por tanto, una pregunta natural que surge es si existe alguna relación entre el error relativo y la cantidad de datos disponibles, en donde se esperaría que este disminuyera cuando se tienen más datos. El comportamiento del tamaño de los conjuntos de entrenamiento en cada proyecto puede observarse en el anexo \ref{train_set_behaviour}. A continuación, se muestra la relación entre el tamaño del conjunto de entrenamiento y el error relativo para el pronóstico que tiene la combinación de los atributos:

\begin{figure}[H]
    \centering
        \includegraphics[width=.8\linewidth]{images/res12_tags_linear.png}
        \caption{Correlación entre tamaño de dataset y el error}
        \label{res12}
\end{figure}

Se puede observar una relación directamente proporcional entre el error y el tamaño del dataset, lo cual va en contra de lo que inicialmente se esperaría. Esto es debido a que una de las limitantes más importantes en la estimación de cada proyecto es la baja cantidad de historias de usuario históricas disponibles que conforman el conjunto de entrenamiento completo. Es lógico esperar que a medida que aumente el tamaño del conjunto de entrenamiento, disminuya el error asociado a la falta de datos. No se tiene una explicación muy clara respecto a este fenómeno, pero si se puede entrar a sospechar que podría estar ocurriendo un caso de \textit{negative transfer}.

\subsection{Distribuciones obtenidas}

El framework genera distribuciones cuando se realiza en pronóstico de un proyecto en particular. A continuación, se presentan las distribuciones de los proyectos pronosticados con los atributos base en la iteración $6$:

\begin{figure}[H]
    \centering
        \includegraphics[width=\linewidth]{images/res15_no_tags_distribution.png}
        \caption{Distribuciones del valor esperado del estimador para los proyectos de la iteración 6 en particular utilizando solo los atributos base. La línea vertical azul corresponde al valor entregado por el estimador al considerar el voto de todos los predictores base. La linea verde, por otro lado, corresponde al valor del esfuerzo real}
        \label{res15}
\end{figure}    

El rendimiento de estas distribuciones no es fácil de medir, debido a que no se conoce la distribución real del valor esperado del estimador. Por lo tanto, al evaluación es más cualitativa que cuantitativa.

Se puede observar que en general las distribuciones obtenidas son representativas del valor promedio obtenido por los métodos de ensamblado, representando adecuadamente también el error del proceso de pronóstico en la visualización presentada. Además, es posible observar que, en general, el \textit{framework} tiende a entregar pronósticos que subestiman el esfuerzo real necesario para terminar los proyectos. Esto tiene sentido, considerando que es la estimación del comité (\textit{total\_points}) la que tiene mayor predominancia por sobre los demás atributos, ya que \textit{Planning Poker} tiende a subestimar historias de usuario en la práctica.