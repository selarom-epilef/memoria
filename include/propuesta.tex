\chapter{Solución Propuesta}
Para poder abordar la problemática planteada, se propone una solución que consiste en la definición de un \textit{framework} de pronóstico de proyectos que se basa en atributos de las historias de usuario que los componen. El flujo de pronóstico se puede resumir en el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=.5\linewidth]{images/framework_diagram.png}
        \caption{Definición general del \textit{framework} propuesto.}
        \label{framework_diagram}
\end{figure}

Este framework tiene como objetivo brindar un pronóstico de un proyecto en particular en base a los datos históricos de otros proyectos disponibles en el momento. Está compuesto por 3 etapas fundamentales, que corresponden a una etapa de preprocesamiento, etapa de selección y una etapa de pronóstico. En cada una de estas etapas se proponen una serie de particularidades que se detallan más adelante y que se ven argumentadas a través de ciertos procesos y actividades de soporte. Estas actividades se pueden visualizar de mejor manera en el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=.8\linewidth]{images/supporting_framework_activities.png}
        \caption{Actividades de soporte principales realizadas para la definición del \textit{framework}.}
        \label{supporting_framework_activites}
\end{figure}

A continuación, se define cada una de las etapas en detalle, asociando además las actividades de soporte realizadas.

\section{Etapa de pre-procesamiento}
En la etapa de pre-procesamiento del \textit{framework}, se toman las historias de usuario finalizadas hasta el momento en que se quiere predecir un proyecto específico, que de ahora en adelante denotaremos por $P_i$, sobre las cuales se realiza un proceso de normalización y filtrado de sus atributos. Para poder definir los procesos concretos que suceden en esta etapa, es necesario definir primero qué información de las historias de usuario es relevante para pronosticar el esfuerzo del proyecto que componen. Para esto, se realiza un análisis de la información original con la que vienen dichas historias y un análisis de posible información adicional que podría ser interesante explorar. Luego, se define un método para poder almacenar la nueva información de las historias y una forma de poder acceder a ella, para así combinarla con la original y utilizarla para los pronósticos.

\subsection{Análisis de atributos \label{subsec_attributo_analysis}}
Entre las fuentes de información que se utilizan para el análisis de atributos se considera la documentación de requerimientos de los proyectos, modelos de datos de sistemas, modelos de arquitectura, \textit{changelogs}, mensajes de \textit{commits} y archivos de configuración generales de servicios. Otra fuente, por otro lado, corresponde a la información provista por las personas que trabajaron históricamente en los proyectos que se utilizarán para realizar la validación de la propuesta.

\subsubsection{Atributos en bruto}
Las historias de usuario disponibles en la plataforma de gestión de proyectos \textit{Taiga} y que en general están disponibles en las plataformas modernas de gestión (tales como \textit{JIRA} \cite{jira}) vienen con los atributos de nombre, estimación inicial del comité de \textit{planning poker}, esfuerzo real, distribución del esfuerzo por cargo, descripción, \textit{sprint} del proyecto a la que pertenece y categoría de historia de usuario definidas por proyecto. A continuación se analiza cada uno de los atributos y su posible utilidad a la hora de realizar predicciones de esfuerzo.
\begin{itemize}
\item \textbf{Nombre}: Si bien es posible argumentar que un algoritmo de predicción puede aprender ciertos patrones de nombres de historias de usuario que impliquen más o menos esfuerzo requerido por el equipo, no existe un lenguaje común entre cada proyecto respecto a cómo deben ser redactados dichos nombres. Esto hace que el atributo no sea útil para las predicciones considerando que cada proyecto tiene un contexto demasiado distinto.
\item \textbf{Estimación inicial del comité}: La estimación inicial del comité corresponde a un número decimal llamado punto de historia de usuario en donde cada uno equivale a aproximadamente 4 horas de trabajo. Este atributo es fundamental para las predicciones del esfuerzo puesto a que durante la asignación de los puntos ocurre un intercambio de conocimiento y opinión entre los miembros del comité evaluador, en donde se llega a un consenso a través del \textit{Planning Poker}. Durante este proceso de interacción humana, se considera el contexto del proyecto, el modelo de negocio del cliente, los requerimientos no funcionales de la historia, las herramientas y tecnologías que se utilizarán durante el desarrollo, los miembros que conformarán el equipo de desarrollo, la experiencia de cada uno de los miembros del comité, información referente al cargo de cada uno de los miembros del comité y otro tipo de información que no es posible rescatar sin que se necesite grabar o transcribir la conversación entera debido a la naturaleza de los diálogos humanos. Todo esto se ve últimamente reflejado en un número decimal cuyo valor es muy alto para los modelos predictivos, puesto que esconde el consenso del proceso de estimación inicial y no refleja toda la conversación ocurrida en el proceso.
\item \textbf{Esfuerzo real}: Este atributo corresponde al número total de horas humanas que tomó completar la historia de usuario. Esto incluye la realización inicial de la historia, como también todo el tiempo invertido en diseño y aseguramiento de calidad. Este atributo es fundamental puesto que será usado para calcular el error de las predicciones de los distintos modelos a utilizar.
\item \textbf{Distribución por cargo}: Corresponde al desgloce de la estimación inicial del comité por cada cargo.

\begin{figure}[H]
\centering
    \includegraphics[scale=0.55]{images/role_list.png}
    \caption{Listado de roles entre los cuales es posible distribuir esfuerzo.}
    \label{role_list}
\end{figure}

La distribución por cargo pueden llegar a ser útil en el proceso de predicción, ya que existen tareas cuya duración y esfuerzo requerido varían según el mismo. Por ejemplo, La distribución en un único cargo puede fácilmente implicar que para una historia de usuario en particular no fue necesario realizar un proceso de integración entre sistemas, lo cual se puede traducir en un aumento de la velocidad de desarrollo.
\item \textbf{Sprint del proyecto}: El principal problema con este atributo es que las etapas no siguen una estructura lógica o estándar y responden solamente a las necesidades y priorizaciones que tiene y realiza el cliente. Es incluso deseable que las historias de usuario puedan no estar asociadas a una etapa aún para poder ser predichas. Por tanto, el atributo no es relevante para las predicciones.
\item \textbf{Categoría}: La categoría de una historia de usuario depende completamente del proyecto, puesto a que se construyen dentro del dominio del mismo proyecto. Pueden incluso existir proyectos sin categorías definidas en donde las historias de usuario solo están asociadas a una sola. Por tanto, se decide no utilizar el atributo puesto a que entorpece el aprendizaje de los distintos modelos a utilizar.
\item \textbf{Descripción}: A partir de la descripción de una historia de usuario es posible obtener gran cantidad de información relevante acerca de ella, incluyendo lógica del modelo de negocio, herramientas y prácticas utilizadas en la realización de la historia de usuario, requerimientos no funcionales y/o sub-requerimientos que pueden estar contenidos en la historia de usuario. Sin embargo, en la plataforma \textit{Taiga} existe una gran cantidad de historias sin descripción. Además, aquellas que sí poseen descripción no siguen un formato único y estándar, por lo que en ellas falta información que en otras sí puede estar.

\begin{figure}[H]
    \centering
    \centerline{%
        \includegraphics[width=0.5\textwidth]{images/short_description.png}%
        \includegraphics[width=0.5\textwidth]{images/long_description.png}%
    }%
    \caption{Diferencia entre la calidad de la descripción de las historias de usuario. A la izquierda se puede observar una descripción pequeña y sin mucha cantidad de información, y a la derecha una descripción más extensa que contiene muchos más detalles gracias a la utilización del lenguaje de redacción \textit{markdown}.}
    \label{description_difference}
\end{figure}

Luego, todas las descripciones entregan información distinta acerca del proyecto, lo podría hacer que los modelos predictivos interpreten incorrectamente que la falta de una palabra clave específica en una historia de usuario implique la inexistencia del uso de una librería, framework o práctica de desarrollo. De forma menos abstracta, se puede ejemplificar lo anterior cuando se tienen historias de usuario que se realizaron con el mismo framework, pero que en donde una tiene el framework en cuestión en su descripción, mientras que la otra tiene una descripción vacía. Esto podría hacer que la información adicional de la descripción forme un sesgo erróneo en los modelos.

Una posibilidad para reparar este atributo y utilizarlo en las predicciones es estandarizar la descripción de las historias de usuario pasadas y llenar la descripción de aquellas que están vacías junto a las personas que las realizaron. El problema de lo anterior es que no están disponibles todas las personas que históricamente participaron en dichas US, y es muy poco probable además poder completar con el mismo nivel de detalle una historia de usuario que tiene más de 2 años de antigüedad.

Por todo lo anterior, parece mejor no utilizar la descripción y añadir de forma manual la información acerca de prácticas, requerimientos no funcionales y librerías a través del método propuesto posteriormente.
\end{itemize}
\subsubsection{Atributos propuestos}
Luego del análisis de los atributos anteriores, se puede observar que debido a que no se utilizará la descripción de las historias de usuario para las predicciones, existe una gran cantidad de información que se está perdiendo. Por otro lado, se tiene que en la literatura se nombran una serie atributos denominados como claves en los atrasos de los proyectos de software. En búsqueda de aumentar la calidad de las predicciones realizadas, se revisan una serie de atributos relevantes y factibles de obtener para proyectos completamente nuevos.

A nivel de proyecto, se definen una serie de atributos que serán propagados a todas las historias de usuario:
\begin{itemize}
\item \textbf{Equipo}: \textit{Cantidad de personas asignadas al proyecto}. La cantidad de personas que son parte de un equipo es un atributo relevante, considerando por sobre todo que mientras más grande es el equipo, más compleja es la dinámica interna que ocurre en ellos. Para poder transparentar esta dinámica no trivial, es pertinente utilizar este atributo.
\item \textbf{Equipo}: \textit{Horas semanales planificadas}. Teóricamente, si se tiene una mayor cantidad de horas semanales asignadas al proyecto por parte del equipo, este debería requerir más esfuerzo. Como no se tiene un atributo referente a la duración máxima del proyecto, es necesario mostrarle a los modelos predictivos de alguna forma esta relación.
\item \textbf{Planificación y control}: \textit{Metodología de desarrollo}. Si bien la metodología de desarrollo en la empresa es ágil, existen ciertos proyectos en donde se prefirió realizar una planificación en cascada considerando que eran proyectos inamovibles o pequeños.
\item \textbf{Planificación y control}: \textit{Prácticas planificadas}. La utilización de ciertas prácticas como \textit{Test-Driven development} hacen que el proceso de desarrollo de software tenga un costo de esfuerzo mucho menor que si no se utilizan. Otra práctica muy común en la industria y que reduce bastante el esfuerzo requerido es la utilización de integración continua.
\item \textbf{Planificación y control}: \textit{¿Es un refactory de un sistema previo?} Trabajar sobre un sistema previo puede significar un aumento en el esfuerzo requerido para completar el proyecto en situaciones en que dicho sistema tiene varios componentes que no pueden ser reutilizados \cite{fowler2018refactoring}.
\item \textbf{Cliente}: \textit{Cantidad de proyectos previos con el cliente}. Uno de los aspectos más importantes en el desarrollo de software es poder educar adecuadamente al cliente sobre los procesos que giran entorno a la implementación de los sistemas. Que el cliente sea alguien que ya ha hecho proyectos con la empresa implica una disminución del esfuerzo requerido en dicho proceso educativo.
\item \textbf{Arquitectura}: \textit{Arquitectura utilizada en el proyecto}. La arquitectura utilizada en un proyecto es uno de los atributos que más impacta en el esfuerzo requerido para finalizar los proyectos. Por ejemplo, una arquitectura de micro-servicios requiere la implementación y unión de múltiples sistemas pequeños, mientras que en una arquitectura monolítica el sistema es uno solamente.
\end{itemize}

Por otro lado, se proponen además los siguientes atributos que están a nivel de historias de usuario:
\begin{itemize}
\item \textbf{Requerimientos}: \textit{Documentación con la que cuenta la funcionalidad}. La documentación referente a diseño de interfaces, procesos, modelos de datos, de arquitectura, de conexiones y mucha más es relevante en cada historia de usuario. Empíricamente, se tiene que cuando no existe documentación clave a la hora de desarrollar las historias de usuario, estas pueden quedar incompletas y por tanto, el esfuerzo total aumenta al tener que realizar retrabajo.
\item \textbf{Requerimientos:} \textit{Requerimientos no funcionales}. Los requerimiento no funcionales son relevantes, ya que generalmente están asociados a una cantidad de esfuerzo mayor. A modo de ejemplo, el requerimiento no funcional de seguridad implica aumentar el esfuerzo requerido para una funcionalidad en particular, al tener que invertir tiempo en investigar cómo mejorar la seguridad, probarla y realizar decisiones a nivel de implementación.
\item \textbf{Usuario}: \textit{Tipo de usuario objetivo que afecta la funcionalidad}. Cuando los sistemas tienen más de un tipo de usuario, el esfuerzo para una funcionalidad en particular tiende a implicar más esfuerzo que el inicialmente previsto. Se propone como sistema de categorización de usuario las categorías de \textit{end-users}, \textit{Administrative entity} y \textit{Super-   admin} que engloban en su mayoría los usuarios tipo que interactúan con los sistemas en la industria.
\item \textbf{Complejidad}: \textit{Escala de complejidad}. La escala de complejidad es la definida en la investigación realiza por Shahid et al. en \cite{ziauddin2012effort}
\item \textbf{Complejidad}: \textit{Tecnologías relevantes utilizadas}. Dependiendo de la tecnología que se utiliza, se pueden tener distintos resultados de esfuerzo, ya que ciertas funcionalidades son más rápidas y fáciles de utilizar en una tecnología y otra. Este atributo solo considera tecnologías relevantes, y no revisa tecnologías más específicas como librerías utilizadas.
\item \textbf{Complejidad}: \textit{Estilos, patrones o tácticas arquitectónicas relevantes utilizadas}. Corresponden a patrones arquitectónicos que pueden ser detectados antes de que inicie la implementación de las funcionalidades. Por ejemplo, la utilización de algún patrón de estilo de código específico para la funcionalidad, la utilización de \textit{websockets}, etc.
\item \textbf{Complejidad}: \textit{¿Existen tecnologías relevantes nuevas (para el equipo) a utilizar?} Este atributo mide si el equipo de desarrollo se enfrentará a tecnologías con las cuales nunca ha trabajado anteriormente. Particularmente para este atributo es fácil ver que cuando una contraparte humana se enfrenta a un proceso completamente nuevo, existe una diferencia de esfuerzo adicional que se debe considerar.
\item \textbf{Complejidad}: \textit{¿Existe vinculación a otros sistemas?} La vinculación con otros sistema tiende a requerir la interacción con proveedores, la cual puede o no ser eficiente dependiendo del tipo de problema, del proveedor y de la calidad de los canales de comunicación. En general, se aprecia un esfuerzo adicional al presentarse este contexto.
\item \textbf{Complejidad}: \textit{¿Es una sección nueva en términos de front-end?} Se define como una sección a un módulo de interfaz gráfica o con implementación mayoritariamente en el frontend. Está relacionado con la distribución de trabajo en frontend de los atributos originales.
\item \textbf{Complejidad}: \textit{¿Es una sección nueva en términos de back-end?} Se define como una sección en backend como un módulo de lógica o procedimiento en donde la mayoría de la implementación es realizada en el backend. Está relacionado con la distribución de trabajo en backend de los atributos originales
\item \textbf{Complejidad}: \textit{Listado de actividades de alto nivel que desarrolla la funcionalidad}. Con el objetivo de poder traer la información de las descripciones inconsistentes al \textit{framework}, se define este atributo cuyos valores categóricos corresponden a actividades genéricas que realizan los usuarios. Por ejemplo, el llenar un formulario, ver un video o eliminar entidades.
\end{itemize}


\subsection{Agregación de atributos}
Para asegurar una independencia entre los atributos propuestos para las predicciones y las plataformas de gestión que se utilizan en la industria (que en el caso particular de la empresa en donde se está validando el método corresponde a \textit{Taiga}), se propone un sistema de agregación de los datos que utiliza la metáfora de \textit{tags} en su interfaz gráfica. Cada \textit{tag} corresponde al valor de una variable categórica específica, en donde los \textit{tags} de proyectos se propagan a las historias de usuario, siendo estas últimas el output del sistema.

La plataforma utiliza el patrón de arquitecura \textit{Model-View-Viewmodel} o \textit{MVVM} cuyas ventajas con respecto a otros modelos más antiguos como \textit{Model-View-Controller} se pueden evidenciar en \cite{garofalo2011building}, en donde se destaca la ventaja de que se tiene la tecnología de la capa de presentación y la tecnología de la capa lógica totalmente desacopladas. Lo anterior permite un mayor grado de escalabilidad y de modificabilidad de la arquitectura que permite realizar una migración a arquitecturas más complejas de manera más fácil. De ahora en adelante, entiendase por \textit{frontend} a la capa de presentación de la plataforma en donde está la interfaz visual, y a \textit{backend} a la capa de lógica en donde se encuentra la lógica de negocio, de almacenamiento y la conexión a la base de datos no relacional. La arquitectura de la plataforma se puede visualizar en el siguiente esquema:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/platform_arquitecture.jpeg}
    \caption{Arquitectura utilizada para la implementación de la plataforma de agregación de atributos (MVVM).}
    \label{platform_arquitecture}
\end{figure}


Para poder añadir la información extra y combinarla eventualmente con información proveniente de las plataformas de gestión que se usen, se debe realizar un proceso investigativo para recopilar los valores de los atributos definidos anteriormente y colocarlos en la plataforma, proceso al cual se le referenciará como \textit{Tagging} de ahora en adelante. Este proceso puede llegar a ser muy engorroso si la plataforma no es lo suficientemente simple, por lo que se le dió énfasis a la interfaz en ser lo más eficaz posible y que entregue herramientas de ayuda para quien realiza el proceso.

La plataforma contiene la lista de todos los proyectos de la empresa, que se encuentra resumida en un dashboard inicial de bienvenida. Para cada proyecto se muestra el porcentaje de historias de usuario que han sido etiquetadas, e información específica referente a los mismos tags.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/platform_home.png}
    \caption{Información resumida de cada proyecto en el dashboard inicial.}
    \label{platform_home}
\end{figure}

Al acceder a un proyecto en particular, se muestra un formulario en el cual se pueden añadir, modificar o eliminar tags a nivel del proyecto completo. Estos tags son eventualmente propagados a las historias de usuarios durante las prediccionnes.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/platform.png}
    \caption{Vista de un proyecto particular en la plataforma de agregación.}
    \label{role_list}
\end{figure}

Abajo del formulario de etiquetas del proyecto completo, se muestra una lista con todas las historias de usuario del proyecto que hayan sido asociadas a etapas específicas del mismo. Esto es de gran importancia, ya que pueden existir historias de usuario creadas que siempre estuvieron en el \textit{backlog} del proyecto y que nunca fueron parte de una etapa o versión específica que implicara esfuerzo por parte del equipo.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/platform_us_list.png}
    \caption{Listado de historias de usuario de un proyecto en donde se registra esfuerzo.}
    \label{platform_us_list}
\end{figure}

Cada una de las historias de usuario contiene un formulario  en donde se pueden modificar los atributos de historias de usuario que fueron definidos en la sección \ref{subsec_attributo_analysis}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/platform_us_tags.png}
    \caption{Formulario de atributos de una historias de usuario en particular.}
    \label{platform_us_tags}
\end{figure}


La plataforma de agregación actúa luego como una fuente de información que es concatenada con la información original de las historias de usuario documentadas en las distintas plataformas de gestión. Esto se realiza a través de la disponibilización del servicio por medio de un \textit{endpoint} REST interno.

\subsection{Definición de conjuntos \label{set_keep_in_mind}}
Es muy importante tener en claro que los proyectos que se realizan en empresas de desarrollo de software tienen una componente temporal asociada, lo que hace que la obtención de los conjuntos de entrenamiento y de validación no puede realizarse de manera arbitraria.

Dado un conjunto de proyectos $P$ que se conoce han finalizado y un proyecto particular al cual se le desea realizar una estimación de esfuerzo $P_i$, es necesario definir el conjunto de entrenamiento como todas aquellas historias de usuario cerradas hasta el momento en que comienza dicho proyecto. Esto debido a que si no se considera dicha restricción, se estaría estimando el proyecto $P_i$ con proyectos $P_j$ con $j > i$ que están en el futuro. Lo anterior se podría definir como una forma de hacer trampa en la etapa de entrenamiento, puesto que cuando se lleve el algoritmo a la práctica y se desee estimar un proyecto nuevo, este no tendrá forma de ver los proyectos que se realizarán en el futuro en la empresa.

\begin{figure}[H]   
    \centering
    \includegraphics[width=0.5\textwidth]{images/dataset_definition.jpg}
    \caption{Ejemplo de la definición del conjunto de entrenamiento y de pruebas. Para estimar el proyecto $P_3$, se deben considerar todas las historias que esten completas antes del tiempo $t_1$, siendo este conjunto $U_{train} = \{U_1, \cdots, U_n, U_1, U_2, \cdots, U_{p-1}\}$, mientras que el conjunto de pruebas corresponde a la historias de usuario del proyecto $P_3$, correspondiente a $U_{test} = \{U_1, \cdots, U_q\}$.}
    \label{dataset_definition}
\end{figure}

Para efectos del presente trabajo, denotaremos al conjunto de entrenamiento como el conjunto de historias de usuario definidas por el criterio anteriormente descrito como $X_{train}$, y el conjunto de pruebas como $X_{test}$.

\subsection{Normalización y filtrado}
El conjunto de datos en bruto que se utiliza como entrada al framework es el listado de historias de usuario provista por el \textit{backend} anteriormente definido, y por el listado de historias provenientes de la plataforma de gestión particular que se esté utilizando. El conjunto completo de historias de usuario es entonces el listado de todas las historias, cuyos atributos corresponden a una concatenación de los atributos originales y los añadidos en la plataforma de \textit{tags}. El formato inicial de los atributos no es compatible con algoritmos de aprendizaje automático, por lo que es necesario realizar un pre-procesamiento definido a continuación:

\begin{enumerate}
\item En bruto, todos los tags corresponden a valores de comportamiento categórico, independiente del tipo de dato que están representando. Por ejemplo, la categoría de `Complejidad' de una historia de usuario tiene inicialmente valores categóricos $\{1 ,2 ,3, 4, 5\}$, siendo que en la práctica es un atributo numérico en donde la magnitud de las variables tiene influencia en el valor de la estimación. Este tipo de atributos es convertido desde categórico a numérico, quedando entonces su dominio como $\mathbb{R}$.
\item Siguiendo lo mismo que en el paso anterior, aquellos tags de índole booleana son transformados a atributos que viven en el dominio $\{0, 1\} \subset \mathbb{R}$.
\item A continuación, los tags que son realmente categóricos como por ejemplo las tecnologías relevantes utilizadas en el proyecto en donde puede haber más de una activa, se transforman a vectores \textit{One-Hot} en donde el atributo original se transforma en $N$ atributos nuevos, donde $N$ es el número de valores distintos posibles para el atributo original y donde el atributo nuevo $A_i$ tiene su dominio en $\mathbb{B}$.
\item Por último, se aplica un método de filtrado simple en donde se eliminan aquellos atributos cuya varianza es 0. Una varianza con este valor se traduce en que el atributo en cuestión tiene el mismo valor en todas las observaciones. Esto hace que el atributo no otorgue información a los modelos y se vuelva completamente irrelevante para la predicción. Realizar este filtrado inicial es importante para asegurar de no caer en problemas debido a la maldición de la dimensionalidad.
\end{enumerate}

\section{Etapa de selección}
Dado que el conjunto de historias de usuario de entrenamiento cambia y aumenta a medida que se van pronosticando nuevos proyectos en el tiempo, cada pronóstico individual se convierte en un problema único de aprendizaje automático por sí solo. Por otro lado, se tiene como objetivo aumentar la precisión del proceso propuesto lo más posible para cualquier tipo de proyecto. Ambos antecedentes hacen necesario que el \textit{framework} se adapte al conjunto de entrenamiento que se le presenta y elija el modelo que entregue los mejores resultados para cada uno de los proyectos a pronosticar.

Antes de definir el conjunto de modelos tentativos con sus respectivos potenciales hiperparámetros sobre los cuales el \textit{framework} decidirá realizar el pronóstico, es necesario definir cómo se realiza la selección de ellos. Esta selección está definida de forma genérica y funcionará para cualquier conjunto de modelos que se deseen explorar.

Para efectos explicativos, sean $M_1, \cdots, M_n$ el conjunto de $n$ modelos distintos que se desean explorar en la selección, en donde por ejemplo $M_1$ podría corresponder a una regresión logística. Sea además $C_1^i, \cdots, C_{p_i}^i$ el conjunto de $p_i$ configuraciones de hiperparámetros para el modelo $M_i$ que se desean explorar.


\subsection{Selección de hiperparámetros}
Cada uno de los modelos del conjunto que se desea explorar es sometido a un proceso automático de tuneo de parámetros, el cual consiste en la generación de un espacio de búsqueda definido por la combinación de todos los valores de los hiperparámetros que se desean explorar y la ejecución de una heurística \textit{greedy} para la obtención de la configuración de hiperparámetros óptimo. Cabe notar que este proceso es computacionalmente costoso debido a que la cantidad de configuraciones distintas sigue un aumento factorial.

El rendimiento de una configuración $C^i_j$, con $j = 1, \cdots, p_i$ está definida según el error cuadrático promedio (MSE) que se obtiene al ponderar el error en un proceso de \textit{3-Fold cross validation} realizado sobre el conjunto de entrenamiento total $X_{train}$. Se selecciona entonces aquella configuración que tenga el menor error de entre todas las configuraciones. El proceso se puede ver resumido en el siguiente diagrama:

\begin{figure}[H]
    \centering
        \includegraphics[width=.5\linewidth]{images/hiperparams_selection.png}
        \caption{Diagrama que describe el proceso de selección de atributos.}
        \label{framework_diagram}
\end{figure}

\subsection{Selección de modelos}
El proceso de selección de modelos es muy similar al descrito para la selección de hiperparámetros. En este caso, se tienen todos los modelos que se desean explorar con sus configuraciones de hiperparámetros óptimas respectivas. Se obtiene el error de cada uno de los modelos haciendo otro \textit{3-Fold cross validation} sobre el mismo conjunto de entrenamiento $X_{train}$, seleccionando aquel que presenta el error más bajo. Se podría argumentar otro camino factible que consiste en dividir el conjunto de entrenamientos $X_{train}$ definido anteriormente en dos nuevos subconjuntos: $X_{model} \subset X_{train} $, con el cual se podrían entrenar todos los modelos predictivos candidatos, y $X_{val} \subset X_{train}$ en donde se validaran los candidatos para encontrar aquel con mejor rendimiento. Cabe notar que en este método hipotético se tendría que $X_{model} \neq X_{val}$ y que $X_{model}$ contendría el $85\%$ de los datos y $X_{val}$ el $15\%$. El problema de esta forma de realizar la validación, es que generalmente el conjunto de validación es demasiado pequeño cuando se tiene un número de observaciones más pequeño y además genera resultados muy distintos cuando se tomen historias significativamente más largas que las demás dentro de cada conjunto. Por otro lado, \textit{cross-validation} es especialmente más efectivo a la hora de validar conjunto de datos con pocas observaciones \cite{bro2008cross}.


\subsection{Modelos propuestos}
Es posible realizar generalizaciones respecto a los modelos que pueden ser más efectivos según las características de las historias de usuario. Si se considera la combinación de los atributos originales y los atributos propuestos, se tiene que un gran pocentaje de ellos corresponden a atributos discretos entre $0$ y $1$, a los cuales se les puede referir también como atributos booleanos. Este tipo de atributos genera problemas para modelos lineales que utilizan algún tipo de distancia dentro de su implementación, debido a que se intenta compatibilizar distancias reales con discretas. Por tanto, la primera condición que deben satisfacer los modelos candidatos es el manejo efectivo de atributos categóricos convertidos por una transformación \textit{One-hot} a atributos booleanos, sin que estos produzcan ruido durante la predicción.

Para poder validar los nuevos atributos de las historias de usuario propuestos, es necesario además utilizar modelos que entreguen información respecto al uso de los atributos de los datos de entrenamiento. Modelos lineales como una $SVR$ que realizan transformaciones de los datos originales o de su espacio vectorial original no permite transparentar el manejo de los atributos de entrada. Por tanto, y considerando la primera condición establecida, son los modelos basados en árboles de decisión los que definen a los modelos candidatos.

Uno de los principales problemas conocidos de los árboles de decisión y sus variantes es que tienden a experimentar \textit{overfitting} fácilmente. Para poder reducir este sobreajuste, se utilizaran árboles de decisión simples como base para distintos métodos de ensamblado, lo que permite reducir el sobreajuste sin perder mucha precisión. Finalmente, los modelos propuestos corresponden a ensamblados de árboles, donde se tienen como modelos candidatos a \textit{Random Forest}, \textit{AdaBoost} y \textit{Extreme Random Forest}. El espacio de búsqueda de hiperparámetros de cada uno de los modelos se define a continuación:

\begin{table}[H]
    \centering
    \begin{tabular}{|p{4cm}|p{2.5cm}p{4cm}p{2cm}|}
    \hline
    Modelos                                & \multicolumn{1}{l|}{Hiperparámetro} & \multicolumn{1}{l|}{Explicación}     & Valores \\ \hline
    \multirow{3}{*}{Extreme Random Forest} & n\_estimators                       & Numero de árboles en ensamblado      & 300     \\
                                           & max\_depth                          & Máxima profundidad del árbol         & 2, 3, $\cdots$, 15       \\
                                           & min\_samples\_leaf                  & Numero de observaciones en nodo hoja & 2, 3, $\cdots$, 20       \\ \hline
    \multirow{3}{*}{Random Forest}         & n\_estimators                       & Numero de árboles en ensamblado      & 300     \\
                                           & max\_depth                          & Máxima profundidad del árbol         & 2, 3, $\cdots$, 15       \\
                                           & min\_samples\_leaf                  & Numero de observaciones en nodo hoja & 2, 3, $\cdots$, 20       \\ \hline
    \multirow{3}{*}{AdaBoost}              & n\_estimators                       & Número de árboles en el ensamblado                                     &  50, 100, $\cdots$ , 200       \\
                                           & learn\_rate                         & Controla cuán rápido incrementan o disminuyen los pesos de las observaciones en el ensamblado                                  & 0.05, 0.10, $\cdots$, 0.95         \\
                                           & loss                                & Función de perdida calculada                                  & lineal, cuadrática, exponencial        \\ \hline
    \end{tabular}
    \caption{Distintas configuraciones de hiperparámetros utilizadas.}
    \end{table}



\section{Etapa de pronóstico}
El modelo $M$ seleccionado en la etapa anterior, corresponde a un predictor del esfuerzo de una historia de usuario particular. Para poder predecir el proyecto entero, es necesario recordar que el esfuerzo de toma desarrollar un proyecto $P$ corresponde a la suma del esfuerzo individual de cada historia de usuario que lo compone. Esto se traduce a la ecuación

\begin{equation}
    E_P = \sum_1^i E_{U_i}
\end{equation}

Donde $E_p$ es el esfuerzo requerido para completar el proyecto $P$ y $E_{U_i}$ es el esfuerzo requerido para completar la i-ésima historia de usuario $U_i \in P$. Por tanto, basta obtener las predicciones de todas las historias de usuario de un proyecto para estimar el proyecto mismo.

\subsection{Distribución del valor esperado del estimador}
Para poder transparentar la varianza del estimador, se calcula la distribución del valor esperado del estimador. Es importante hacer la distinción entre esta distribución y la distribución de los datos originales, donde la última no es trivial de predecir. Para esto, se utilizando el método de \textit{Kernel density estimation} (Sección \ref{chapter_kernel}), en donde el ancho de banda se determina con el método de la regla de Scott \cite{heidenreich2013bandwidth}. Es importante notar que aunque el método asume distribuciones gausianas al sobreponer las curvas, el resultado en general puede adaptarse correctamente a los datos del estimador. Por otro lado, si bien no es posible realizar suposiciones respecto a la distribución del esfuerzo en un proyecto, si es posible hacerlo cuando se está revisando la distribución del valor esperado de un predictor.
