\chapter{Estado del Arte}

A continuación, se presenta el estado del arte referente a los avances en ingeniería de software para contextualizar el estado actual de la industria y del avance actual sobre el tópico de estimación de esfuerzo de proyectos de desarrollo de software.

\section{Métodos de estimación de esfuerzo}
La estimación de esfuerzo de software es una de las áreas más complejas e importantes del desarrollo y la administración de proyectos de software. La dificultad recae en realizar pronósticos de parámetros al inicio del ciclo de vida del proyecto, en donde el alcance del mismo aún no está completamente claro y en donde la incerteza respecto a las funcionalidades que lo compondrán es muy alta \cite{boehm_1981}. A menudo, la falta de conocimiento sobre factores externos que pueden influenciar al proyecto y riesgos que pueden ocurrir producen que las estimaciones por juicio experto sean imprecisas y terminen siendo demasiado optimistas. Dichas estimaciones tiene un gran impacto en la entrega del software en una ventana de tiempo definida \cite{ALAMI201662}.

La propagación de metodologías ágiles ha logrado disminuir la tasa de fallo de los proyectos de software \cite{chaosreport}, pero esta sigue siendo substancial y observable en el día a día por profesionales del rubro \cite{de2015project}.

Un pronóstico mas preciso al inicio de los proyectos tiene un impacto significativo en la probabilidad de que le proyecto se pueda completar \cite{ALAMI201662}, por lo que se ha hecho un mayor énfasis en este proceso en los últimos años. En las últimas décadas, diferentes modelos de estimación de esfuerzo han sido desarrollados. Las técnicas que se han desarrollado se pueden agrupar en 4 grupos principales, que corresponden a métodos de estimación por evaluación experta, por analogía, con modelos paramétricos y con aprendizaje automático.

\subsection{Estimación por evaluación experta}
En este grupo de métodos de estimación, el predictor corresponde a un ente humano que utiliza su experiencia  en el rubro para pronosticar el esfuerzo de los proyectos de software \cite{jorgensen_2007}, que puede consistir de uno o más actores humanos que utilizan su conocimiento para segmentar potenciales proyectos de software en unidades más pequeñas y atómicas, que generalmente corresponden a casos de uso, requerimientos (funcionales y no funcionales) o historias de usuario. Estos actores estiman la duración total del proyecto en base a las características generales del proyecto, la cantidad de unidades segmentadas, las características de dichas unidades y otros atributos externos o internos a la organización como por ejemplo, las habilidades de los desarrolladores del equipo, la disponibilidad de conocimiento para desarrollar el proyecto dentro y fuera de la empresa, entre otros.

Existen diversas formas de implementar los juicios expertos. La técnica más sencilla que se puede implementar es designar a un solo actor humano con suficiente conocimiento y experiencia para que realice las estimaciones de las funcionalidades del proyecto o del proyecto completo. Esta técnica tiene una gran cantidad de error y sesgo en su implementación debido a que la estimación siempre tendrá el sesgo de la persona que realiza las estimaciones y será muy sensible al error humano \cite{lazarevic2014project}. Por tanto, es más común utilizar técnicas que permitan la participación de más entes humanos en donde la estimación final es normalmente consensuada.

Las técnicas de estimación por evaluación experta más conocidas corresponde al método de Delphi, PERT y Planning Poker.

El método de Delphi es un procedimiento de pronosticación muy antiguo basado en la comunicación directa entre miembros de un panel de expertos. Los expertos responden cuestionarios sistemáticos sobre los cuales se llega eventualmente a consensos
\cite{delphi_technique}.

PERT (\textit{Program Evaluation and Review Technique}) es una técnica que se originó inicialmente como una herramienta para planificar el desarrollo un sistema completo de armamento en EE.UU. La técnica considera un proyecto como una red acíclica de eventos y actividades, en dónde la duración del mismo se define por un plan de flujo de sistema en donde cada funcionalidad tiene un valor esperado de su duración y una varianza asociada. Estos valores se obtienen a partir de un proceso en donde participan uno o más entes humanos, que en base a su experiencia asignan pesos al flujo que se traducen en tiempo de trabajo \cite{pert_technique}.

Planning Poker es la técnica más recomendada para utilizar junto a metodologías ágiles \cite{agile_manifesto} y corresponde a definir que la estimación de los proyectos sea realizada por un grupo de personas expertas, a través de un protocolo específico. Este protocolo consiste en ir por cada historia de usuario individual que compone al proyecto, contextualizarla y permitir que todos los miembros participantes de la instancia realicen una estimación en secreto. Luego, la estimación individual es revelada a todos los demás miembros, y en caso de que todos hayan estimado por igual, se estime con dicho valor. En caso de que no haya consenso en la estimación individual, se abre el debate para justificar el valor que cada uno de los miembros asigno y se repite le proceso hasta lograr consenso. \cite{planning_poker_paper}

Las estimaciones en la metodología de \textit{Planning Poker} se hacen generalmente con puntos que son interpretados por el equipo, aunque existen también versiones que utilizan la metáfora de tallas S, M, L y XL para definir el esfuerzo de una historia de usuario \cite{planning_poker_paper}. En aquellas sesiones en dónde se utilizan puntos, cada uno tiene una representación consensuada de unidad de esfuerzo. Existe varias técnicas para determinar la equivalencia entre un punto de funcionalidad y su equivalencia, en dónde generalmente el comité de expertos que participará utiliza como referencia alguna tarea o funcionalidad base que puede realizar en el plazo de medio día, u otras técnicas más específicas en donde existe una correlación matemática entre puntos y esfuerzo. La escala de dichos puntos es generalmente una que refleje la complejidad creciente de las funcionalidades, en donde generalmente se utiliza el número de Fibonacci para el incremento entre valores.

\begin{figure}[H]
    \centering
    \includegraphics[width=.8\linewidth]{images/planning-poker-cards.png}
    \caption{Ejemplo visual de cartas utilizadas en una sesión de Planning Poker.}
    \label{planning_poker_cards}
\end{figure}


Las técnicas de estimación de esfuerzo basadas en conocimiento experto son mayormente utilizadas por su simplicidad y fácil aplicación \cite{wysocki2011effective}. Sin embargo, estas técnicas tienden a estar sesgadas principalmente por tres razones que causan estimaciones sobre-optimistas e impactan la probabilidad de éxito del proyecto: técnica, psicológica y política. La primera se refiere al sesgo producido por la desinformación de los entes expertos, tanto a riesgos como a factores externos que afectan a las funcionalidades del proyecto. Las otras dos se enfocan en el factor humano, haciendo énfasis en la imposibilidad de planificar factores como la presión ejercida por los clientes, necesidades políticas de demostrar conocimiento y presión por ventanas de tiempo holgadas. Este sesgo puede ser controlado a medida que el equipo de la organización madura a través del tiempo \cite{planning_poker_paper}. Por otro lado, no se es posible cuantificar y justificar todos los factores y variables que son consideradas durante el proceso de pronóstico de manera consistente.

\subsection{Estimación basada en analogías}
Este tipo de estimación se basa en la hipótesis de que proyectos que son fundamentalmente similares entre sí, ya sea por tipo de tecnología usada, tipo de funcionalidades, contextos en los que se construyen o el equipo que los componen, deberían ser relativamente similares en la cantidad de esfuerzo necesario que se debe invertir para completarlos. Lo anterior es una conclusión obtenida a través de la técnica de \textit{Case-Based Reasoning} \cite{kolodner2014case}. En estas técnicas, se infiere una estimación del esfuerzo requerido para completar un nuevo proyecto de software a través de la identificación de un conjunto relevante de atributos que lo describen y que pueden se comparados con otros proyectos. Estos atributos son luego utilizados para encontrar proyectos similares y promediar el esfuerzo promedio de cada uno de ellos.

Existen otras variaciones de técnicas por analogía en donde se busca estimar las unidades fundamentales del proyecto por analogía y similitud según ciertos atributos, para luego calcular el esfuerzo del proyecto con la sumatoria de las predicciones individuales.

La principal ventaja de este tipo de modelos es que son mucho más simples de explicar a usuarios externos, como clientes, stakeholders y unidades internas de administración, en comparación a métodos más cerrados como el uso de redes neuronales en estimación con aprendizaje automático. La otra ventaja, es que estas técnicas pueden ser usadas para modelar relaciones complejas y no lineales entre variables dependientes e independientes \cite{ezghari2018uncertainty}. Sin embargo, la principal desventaja de estas técnicas es que requieren de un gran número de proyectos o unidades fundamentales similares para poder entregar pronósticos certeros. Lo anterior es algo que ocurre raramente en la práctica \cite{POSPIESZNY2018184}.

\subsection{Modelos paramétricos}
En un esfuerzo de poder eliminar los sesgos humanos que existen en las estimaciones basadas en juicios expertos, se han propuesto una serie de modelos paramétricos para lograr estimaciones más precisas. Estos modelos se caracterizan por traducir características del desarrollo de software a parámetros numéricos, que son luego utilizados para calcular el esfuerzo según alguna función matemática.

\subsubsection{COCOMO}

Uno de los primeros modelos en el área de estimación de esfuerzo de proyectos de software corresponde a \textit{COCOMO} del inglés \textit{Constructive Cost Model} \cite{boehm_1981} que utiliza el tamaño de programas de proyectos anteriores para determinar el esfuerzo requerido. El tamaño de los programas es medido en función a la cantidad de líneas de código escritas para su completación. \textit{COCOMO} se puede aplicar a tres clases de software:

\begin{enumerate}
	\item \textit{Proyectos orgánicos}: corresponden a proyectos desarrollados por equipos pequeños con buena experiencia trabajando con requerimientos poco rígidos, que implica requerimientos levemente ágiles en el sentido de que pueden cambiar en el transcurso del proyecto de forma muy leve.
    \item \textit{Proyectos semi-separados}: equipos medianos con experiencia mixta trabajando con requerimientos poco rígidos.
    \item \textit{Proyectos incrustados}: proyectos desarrollados con un conjunto de restricciones fuertes.
\end{enumerate}

El \textit{COCOMO} básico utiliza 3 ecuaciones fundamentales. La primera de ellas es:

\begin{equation}
	E = a_b \cdot (KLOC)^{b_b}
\end{equation}

En donde $E$ corresponde al esfuerzo total necesario que se debe aplicar para completar el programa o proyecto medido en \textit{meses humanos}, $KLOC$ la cantidad de miles de líneas de código necesarias estimadas, $a_b$ y $b_b$ son constantes que dependen del tipo de proyecto y tienen valores pre-calculados según la experiencia transferida desde otros proyectos de software (Tabla \ref{constant_definition}).  La determinación de $KLOC$ tiende a ser bastante difícil y en general no se tiene un proceso formal definido para hacerlo, siendo entonces más común realizarlo desde la experiencia de un ente humano o alguna heurística consensuada.
Dada la estimación del esfuerzo, se puede obtener la duración total del proyecto según la siguiente formula:

\begin{equation}
D = c_b \cdot (E)^{d_b}
\end{equation}

\begin{table}[H]
    \centering
    \begin{tabular}{|p{4cm}p{2cm}p{2cm}|}
        \toprule
        Tipo de proyecto & $a_i$ & $b_i$ \\
        \midrule
        Orgánico & 3.2 & 1.05 \\
        Semi-desacoplado & 3.0 & 1.12 \\
        Incrustado & 2.8 & 1.20 \\
        \bottomrule
    \end{tabular}
    \caption{Valores para las constantes de COCOMO según el tipo de proyecto \cite{boehm_1981}.}
    \label{constant_definition}
\end{table}

En dónde $D$ es la duración total. Por último, \textit{COCOMO} provee una última métrica para determinar la cantidad de personas requeridas para completar el proyecto dada por: 

\begin{equation}
P = \frac{E}{D}
\end{equation}

Una variación más reciente de la técnica anterior corresponde a COCOMO II y utiliza atributos adicionales de los proyectos para calcular el denominado \textit{effort adjustment factor} ($EAF$), que se añade a la fórmula de esfuerzo de la siguiente manera:

\begin{equation}
    E = a_b \cdot (KLOC)^{b_b} \cdot EAF
\end{equation}

El factor anterior se calcula utilizando atributos del producto que se quiere construir, del hardware que se utilizará, del equipo que desarrollará la solución y del proyecto en sí. Cada atributo en particular es asignado a un valor según una tabla de ratings (Tabla \ref{cocomo_ii_attributes}), cuyo producto determina el valor de $EAF$:

\begin{equation}
    EAF = \prod_{1}^{K} k_i
\end{equation}

Donde $k_i$ es el valor asignado al atributo $i$. La gran ventaja de esta nueva versión de COCOMO es que logra considerar atributos importantes dentro de la estimación que no se ven reflejadas con la cantidad de líneas de código del proyecto. Si bien este método tiene mejores resultados en la predicción que la primera versión de COCOMO, éstos siguen siendo muy altos y fuera de márgenes de error tolerables \cite{toka2013accuracy}.

\begin{table}[H]
\centering
\begin{tabular}{|p{4cm}|cccccc|}
    \hline
    & \multicolumn{6}{c|}{Ratings} \\
     Atributos & Muy bajo & Bajo & Nominal & Alto & Muy Alto & Extra Alto \\
     \hline
     \multicolumn{1}{|c|}{\textbf{Producto}} & & & & & & \\
     \hline
     Confiabilidad requerida & 0.75 & 0.88 & 1.00 & 1.15 & 1.40 & \\
     Tamaño de la base de datos & & 0.94 & 1.00 & 1.08 & 1.16 & \\
     Complejidad & 0.7 & 0.85 & 1.0 & 1.15 & 1.30 & 1.65 \\
     \hline
     \multicolumn{1}{|c|}{\textbf{Hardware}} & & & & & & \\
     \hline
     Limitantes de rendimiento & & 1.0 & 1.11 & 1.30 & 1.66 & \\
     Limitantes de memoria & & 1.00 & 1.06 & 1.21 & 1.56 & \\
     Volatibilidad & & 0.87 & 1.00 & 1.15 & 1.30 & \\
     Tiempo de \textit{turnabout} & & 0.87 & 1.00 & 1.07 & 1.15 & \\
     \hline
     \multicolumn{1}{|c|}{\textbf{Equipo}} & & & & & & \\
     \hline
     Capacidad analítica & 1.46 & 1.19 & 1.0 & 0.86 & 0.71 & \\
     Experiencia en aplicaciones & 1.29 & 1.13 & 1.00 & 0.91 & 0.82 &\\
     Capacidad ingenieril & 1.42 & 1.17 & 1.00 & 0.86 & 0.70 & \\
     Experiencia con VM & 1.21 & 1.10 & 1.00 & 0.9 & & \\
     Experiencia en el lenguaje & 1.14 & 1.07 & 1.00 & 0.95 & & \\
     \hline
     \multicolumn{1}{|c|}{\textbf{Proyecto}} & & & & & & \\
     \hline
     Aplicación de metodologías de ingeniería de software & 1.24 & 1.10 & 1.00 & 0.91 & 0.82 & \\
     Uso de herramientas & 1.24 & 1.10 & 1.0 & 0.91 & 0.83 & \\
     Tiempo de desarrollo & 1.23 & 1.08 & 1.0 & 1.04 & 1.10 & \\
     \hline
\end{tabular}
\caption{Valores de los atributos que afectan a COCOMO II. Estos valores han sido calibrados según observaciones de la industria al momento de proponer al iteración del método \cite{cocomo_2}. Aquellas áreas que no tienen valores no poseen una calibración testeada.}
\label{cocomo_ii_attributes}
\end{table}

\subsubsection{SLIM}

Otro modelo paramétrico popular consiste en \textit{Putnam's Software Life-cycle Model} (SLIM). Putnam observó que los perfiles de contratación de desarrolladores seguían una relación con la distribución de Rayleigh (Figura \ref{slim_curve}).

\begin{figure}[H]
    \centering
    \includegraphics[width=.8\linewidth]{images/slim_curve_rayleigh.png}
    \caption{Distribución de Rayleigh obtenido ajustando los parámetros $K = 1.0$, $a = 0.02$ y $t_d = 0.18$ \cite{cocomo_2}.}
    \label{slim_curve}
\end{figure}

La curva de Rayleigh se define por la siguiente ecuación diferencial:

\begin{equation}
    \frac{dy}{dt} = 2Kate^{-at^2}
\end{equation}

Donde las variables $K$, $a$, $t$ y $e$ representan parámetros ajustables de la distribución y dependen de las características que se están utilizando para la predicción.

Este método soporta atributos de entrada como la función de puntos, líneas de código y otros más. En este modelo, se utiliza la productividad para obtener la distribución de la capacidad del equipo de desarrollo (o \textit{Manpower}) a partir de características de los proyectos. Putnam definió la siguientes relación:

\begin{equation}
    \frac{B^\frac{1}{3} \cdot \textnormal{Size}}{\textnormal{Productivity}} = \textnormal{Effort}^\frac{1}{3} \cdot \textnormal{Time}^\frac{4}{3}
\end{equation}

Donde $\textnormal{Size}$ es el tamaño del proyecto medido en líneas de código, puntos de función u otra heurística, $B$ una constante de escalamiento que se utiliza para calibrar el atributo elegido para $\textnormal{Size}$, $\textnormal{Productivity}$ corresponde al factor de productividad de la empresa (también manejado a partir de heurísticas), $\textnormal{Time}$ es el marco temporal en el que se mueve el proyecto (generalmente en años) y $\textnormal{Effort}$ el esfuerzo en horas requerido para realizar el proyecto. Realizando un despeje, se define el esfuerzo en este modelo como:

\begin{equation}
    \textnormal{Effort} = \left [ \frac{\textnormal{Size}}{\textnormal{Productivity} \cdot \textnormal{Time}^\frac{4}{3}}  \right]^3 \cdot B
\end{equation}

\subsubsection{SEER-SEM}

El último método relevante corresponde a SEER-SEM, que utiliza una serie de modelos estadísticos para atributos como tamaño, tecnologías, esfuerzo, costo, defectos, mantención y otros más para modelar el esfuerzo total del proyecto. En este modelo, el esfuerzo 

\begin{equation}
    K = D^{0.4} \cdot {\frac{S_e}{C_{te}}}^{E}
\end{equation}

Donde $S_e$ es el tamaño efectivo del proyecto que se calcula dependiendo del atributo de entrada que se utiliza para la predicción, $C_{te}$ la tecnología efectiva que se calcula capturando una serie de factores relacionados con la eficiencia del equipo o su productividad, $D$ la complejidad del tamaño del equipo y $E$ una variable de entropía que corresponde a una constante (empíricamente se ha demostrado que el valor óptimo es $1.2$).


Los modelos paramétricos tienen varias ventajas con respecto a modelos anteriormente vistos: en general son buenos para realizar estimaciones rápidas de esfuerzo, no tienen el mismo sesgo que tienen los modelos basados en juicio experto y permiten transparentar aquellas características de los proyectos que están siendo consideradas para la estimación . Sin embargo, tienen como desventaja principal que son muy dependientes del lenguaje de programación en el cual se desarrollará el proyecto \cite{POSPIESZNY2018184} y no consideran atributos externos a la programación misma de funcionalidades, como por ejemplo el tiempo invertido en el diseño visual, de interacción y de servicios de la misma. Otra desventaja importante que es necesario considerar a la hora de utilizar este tipo de soluciones es que generalmente estos toman las líneas de código producidas en los proyectos como característica principal. Lo anterior no es muy factible en la industria actual como medida de estimación, ya que existe un gran porcentaje de retrabajo y cambios en la estructura de las funcionalidades de manera mucho más rápida gracias a la incorporación de métodos ágiles \cite{agile_software_development}.


\subsection{Aprendizaje automático}
Dadas las desventajas y deficiencias que se han podido observar en los métodos anteriores, que persisten incluso combinándolos con método de lógica difusa \cite{ezghari2018uncertainty}, se han realizado durante las últimas décadas múltiples investigaciones con el objetivo de realizar estimaciones utilizando algoritmos de máquinas de aprendizaje modernos \cite{WEN201241}. Estos algoritmos han resultado ser muy eficientes para poder manejar el problema de la incertidumbre en la estimación y sus resultados demuestran su potente capacidad predictiva para el esfuerzo de los proyectos, tanto al inicio del ciclo de vida, como en etapas intermedias de este \cite{lopez2012software}. Dado que estos algoritmos se caracterizan por realizar estimaciones automáticas que no requieren de intervención humana, más allá de configurarlos correctamente, se disminuye el sesgo producido en otros tipos de estimación de índole humana, psicológica o política.

Con modelos de máquinas de aprendizaje, se tiene mayor dinamismo respecto a las características que se utilizan para las estimaciones. Esto se debe a que en estos modelos los parámetros no están fijos como en modelos paramétricos y pueden cambiar según los datos que se van observando.

Incluso siendo tan prometedores, estos algoritmos son utilizados nada o muy poco en la industria. La razón detrás de esto corresponde a que los algoritmos estaban siendo entrenados con bases de datos demasiado antiguas, con tecnologías, prácticas y metodologías que no son homologables en los proyectos modernos, cuya metodología principal de ciclo de vida es la de tipo Ágil \cite{POSPIESZNY2018184}.

En el año 2012, Wen et al. \cite{WEN201241} realizó el estudio más completo acerca del estado de la utilización de los algoritmos de máquinas de aprendizaje para realizar estimaciones de esfuerzo. En él, se revisaron 84 estudios cuyos resultados arrojaron que los algoritmos más efectivos corresponden a aquellos en la categoría de redes neuronales, máquinas de soporte y árboles de decisión. En particular, las redes neuronales demostraron tener la ventaja de que no son sensibles a la  distribución de los datos, pero si a la calidad de los atributos utilizados para la predicción y al número de observaciones disponibles \cite{nassif_2016}. Esto no las hace factibles para datasets muy pequeños, en empresas que tienen pocos proyectos en su portafolio.

De toda la investigación que se ha realizado en las últimas década, es posible extraer recomendaciones transversales a todos los modelos que estén en la categoría. El énfasis debe realizarse en el pre-procesamiento de los datos, especialmente en planificar estrategias para manejar outliers, datos incompletos y su correlación. Si bien es necesario analizar los datos y el tipo de contexto en el que se está trabajando, existen una serie de recomendaciones que han sido obtenidas de investigaciones empíricas. Respecto a los outliers,  eliminar aquellos que estén fuera de 3 veces la desviación estándar del conjunto de datos es lo más recomendable para evitar que estos desenfoquen los resultados predictivos del modelo que vayamos a seleccionar. Es recomendado además eliminar observaciones con atributos sin información ya, que utilizarlas podría reducir significativamente la variabilidad de los datos \cite{POSPIESZNY2018184}. Con respecto a la correlación de los datos, es en general buena práctica eliminar aquellos atributos que tengan un grado de correlación muy alto entre sí, para así disminuir la cantidad de parámetros de entrada de los modelos.

Incluso con la eliminación de outliers, datos incompletos y atributos fuertemente correlacionados, existe el hecho de que los algoritmos de máquinas de aprendizaje son muy sensibles al ruido en los conjuntos de entrada, por lo que es necesario utilizar métodos de ensamblado de modelos \cite{minku2013ensembles}. Estos métodos combinan la predicción de varios modelos individuales y generan una única predicción que suele ser más certera. Sin embargo, estos métodos son bastante costosos computacionalmente hablando y se recomienda que de ser utilizados, sean los más simples posible \cite{azhar2013using}.

% Esto respecto a redes neuronales
Si bien los modelos de máquinas aprendizajes mencionados anteriormente son los que muestran mejores resultados, se han investigado una gran variedad de ellos a medida que el área del aprendizaje automático iba progresando en su investigación propia. Se han estudiado una gran cantidad de modelos para \textit{SDEE}. Estos incluyen una gran cantidad de técnicas de regresión (lineales y no lineales), el uso de redes neuronales, aprendizaje por instanciación (\textit{instance-based learning}), modelos basado en árboles de reglas, razonadores basados en casos específicos, aprendizaje lento (\textit{lazy learning}), clasificación Bayesiana utilizando árboles de clasificación, SPM (\textit{Support vector machines)}, entre otros \cite{shepperd_2007} \cite{shepperd_2012}. La mayoría de estos estudios evalúa solo una cantidad limitada de técnicas de modelamiento en un conjunto de datos, lo cual limita la generalización de sus resultados. Además, los resultados de dichos estudios son difíciles de comparar debido a las diversas características de los escenarios en los que son empleados. Por tanto, el problema de qué modelo utilizar sigue aún sin ser respondida \cite{morera_2017}.

En el año 2018, se realizó un estudio que buscaba estandarizar los resultados de distintas investigaciones de aprendizaje automático aplicado a predicción de esfuerzo de proyectos para poder realizar una comparación justa entre ellos \cite{POSPIESZNY2018184}. Este estudio era necesario considerando que todos los estudios publicaban resultados en distintas métricas, o realizaban procesos de pre-procesamiento y ajuste de algoritmos que eran cuestionables si se hacía referencia al estado del arte respecto de los algoritmos de máquinas de aprendizaje. De este estudio se pudo determinar que el algoritmo que mejor se comportó fueron las máquinas de soporte vectorial, en inglés \textit{Support Vector Machines} (SVM). Este resultado no asegura que dicho algoritmo se comporte de igual manera en todos los casos y datasets de proyectos debido a la naturaleza de los algoritmos de aprendizaje. Lo más relevante, es que el uso de métodos ensambladores produce un mejor resultado que los estimadores por sí solos.


\begin{figure}[H]
    \centering
    \includegraphics[width=.8\linewidth]{images/ml_models.png}
    \caption{Cuadro comparativo entre distintos modelos de aprendizaje automático \cite{POSPIESZNY2018184}.}
    \label{ml_models}
\end{figure}

Si se lleva esto a un paso más y se realiza un estudio comparativo con modelos estándar, es claro que el aprendizaje automático es la opción preferente a la hora de querer realmente minimizar la incerteza de los pronósticos de duración de los proyectos.


\section{Características utilizadas para la estimación}
Si bien es importante el tipo de modelo a utilizar para la estimación del esfuerzo de un proyecto de software, es más relevante aún definir los atributos del proyecto que deberían ser considerados a la hora de estimar. Los modelos paramétricos y de juicio experto tienen por lo general un tipo de atributo para realizar la estimación, el cual es rara vez posible de cambiar. Sin embargo, estos atributos deberían ser por lo menos analizados considerando que existen varias investigaciones que respaldan que la consideración de ellos afectan las duraciones de los proyectos informáticos. A continuación se describen los atributos más relevantes utilizados para las predicciones a lo largo del tiempo.

% Lines of code
Los primeros intentos de estimación utilizaron el tamaño de los proyectos desarrollados en función de la cantidad de líneas de código que los componían. Esta unidad como parámetro de entrada para modelos de aprendizaje automático no es factible en la industria moderna, puesto que no considera el retrabajo de funcionalidades debido a calidad insuficiente o por requerimientos que cambian a mitad de camino, tal y como sucede en metodologías del modelo Ágil \cite{agile_software_development}. Además, esta métrica depende demasiado de la tecnología con la que se construirá el software. Lo anterior es incompatible en la industria moderna por modelos nuevos como la arquitectura por micro-servicios, en donde una funcionalidad puede estar compuesta de múltiples sub-servicios con diversos lenguajes de programación \cite{newman2015building}.

% Project units
Posteriormente, se comenzaron a generar modelos que intentaban desacoplar la estimación del esfuerzo con la tecnología utilizada. Para esto, se comenzó a diseccionar los proyectos en unidades mínimas de valor que tomaron el nombre de puntos de funcionalidad. Un enfoque más moderno y compatible con las metodologías ágiles es la utilización del concepto de puntos de historia de usuario, que si bien tiene una discrepancia técnica con el anterior, son en esencia lo mismo. Estos puntos han permitido a modelos de juicio experto poder realizar estimaciones subjetivas según la experiencia de las personas participantes en las sesiones de estimación. El mismo concepto fue utilizado también para modelos paramétricos modernos, como SLIM.

Los enfoques más modernos implican realizar una selección y extracción de atributos inherentes al desarrollo de software para estimar. Los primeros aprontes provienen del modelo COCOMO II \cite{cocomo_2}, en donde se utilizan atributos del producto, del hardware y del equipo de desarrollo a utilizar.

La mayoría de las investigaciones que han utilizado modelos de aprendizaje automático o por analogía como solución para realizar los pronósticos de esfuerzo, son realizadas en la base de datos ISBSG \cite{fernandez2014potential}, que consiste en un repositorio de información sobre proyectos de distinta índole y que almacena más de 8.000 proyectos. Estos atributos hacen referencia a distintas características de proyectos, los cuales se pueden agrupar por la descripción del proyecto en detalle, el tamaño del proyecto y atributos relacionados con el tamaño, el esfuerzo real ejercido para completar el proyecto, los defectos detectados en el proyecto, la planificación del proyecto, la arquitectura utilizada, las técnicas utilizadas, la documentación utilizada y detalles de la plataforma en sí. Estos atributos han sido utilizados por diversos estudios, de los cuales existe una conclusión en común, que se puede resumir en que se debe hacer énfasis en el procesamiento de los datos iniciales para que se adecúen lo mejor posible a los modelos utilizados y a las suposiciones que se hagan \cite{POSPIESZNY2018184} \cite{nassif_2016} \cite{WEN201241}.

Los modelos modernos intentan favorecer datasets con un mayor volúmen de datos para mejorar el poder predictivo de sus estimaciones, sin considerar que datasets como el de ISBSG no incluyen factores humanos importantes, como por ejemplo, la madurez técnica del equipo que desarrolla el software \cite{fernandez2014potential}. Tampoco se tiene una forma de medir correctamente el esfuerzo requerido en términos de diseño visual, de interacción y de servicios que podría llegar a componer una funcionalidad.

\section{Métricas de rendimiento}

En el área de la predicción de esfuerzo, se han estandarizado una serie de métricas para determinar el rendimiento de un método en particular. Estas métricas corresponden al error relativo medio ($MRE$) de las observaciones predecidas, que en el caso del presente trabajo corresponde al error relativo en cada una de las historias de usuario pronosticadas y se define por:

\begin{equation}
    MRE(t) = \frac{|y_t-y_t^p|}{y_t}
\end{equation}

donde $y_t$ es el valor real de la observación y $y_t^p$ es la predecida. La segunda métrica corresponde al error relativo promedio de todos los proyectos ($MMRE$) definido como:

\begin{equation}
    MMRE = \frac{1}{n} \sum^n_{t=1} MRE(t)
\end{equation}

donde $n$ es la cantidad total de proyectos. Y por último, a $PRED(25)$, en donde $PRED(x)$ se define por:

\begin{equation}
PRED(x) = \frac{1}{n} \sum^n_{t=1} \begin{cases}
    1 & \textnormal{si } MRE(t) \leq x \\
    0 & \textnormal{de otro modo}
\end{cases}
\end{equation}

y que se traduce en el porcentaje de proyectos pronosticados que tienen un $MMRE$ bajo el $25\%$. Se dice que un modelo predictivo del esfuerzo de desarrollo de software es de alta calidad cuando se cumple que $PRED(25)$ es mayor a un $80\%$ \cite{pred25}.